package com.mathsystem.repo;

import com.mathsystem.entity.task.AlgAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlgAnswerRepo extends JpaRepository<AlgAnswer, Long> {
}
