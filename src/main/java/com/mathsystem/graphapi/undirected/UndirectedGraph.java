package com.mathsystem.graphapi.undirected;

import com.mathsystem.entity.graph.Color;
import com.mathsystem.entity.graph.Edge;
import com.mathsystem.graphapi.AbstractGraph;
import com.mathsystem.graphapi.Vertex;
import lombok.Data;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Класс неориентированного графа
 */
@Data
public class UndirectedGraph extends AbstractGraph {

    /**
     * Конструктор - создание нового объекта
     * неориентированного графа с определенными значениями.
     * Используется в контроллерах - не используется в плагинах
     * @param vertexCount - количество вершин
     * @param edgeCount - количество ребер
     * @param edges - список ребер
     * @param vertices - список вершин
     */
    public UndirectedGraph(int vertexCount,
                           int edgeCount,
                           List<Edge> edges,
                           List<com.mathsystem.entity.graph.Vertex> vertices) {
        this.vertexCount = vertexCount;
        this.edgeCount = edgeCount;
        this.vertices = new ArrayList<>();

        for (int i = 0; i < vertexCount; i++) {
            com.mathsystem.entity.graph.Vertex v =  vertices.get(i);
            this.vertices.add(new Vertex(
                    Integer.parseInt(v.getName()),
                    v.getName(),
                    v.getColor(),
                    v.getWeight(),
                    v.getLabel(),
                    new LinkedList<>()
            ));
        }

        for (Edge edge: edges) {
            UndirectedEdge undirectedEdge = new UndirectedEdge(
                    this.vertices.get(Integer.parseInt(edge.getFromV())),
                    this.vertices.get(Integer.parseInt(edge.getToV())),
                    edge.getWeight() == null ? 0 : edge.getWeight(),
                    edge.getColor(),
                    edge.getLabel(),
                    edge.getName()
            );

            this.vertices
                    .get(Integer.parseInt(edge.getFromVertex().getName()))
                    .getEdgeList().add(undirectedEdge);
            this.vertices
                    .get(Integer.parseInt(edge.getToVertex().getName()))
                    .getEdgeList().add(undirectedEdge);
        }
    }

    /**
     * Конструктор - создание нового объекта
     * неориентированного графа с определенными значениями из файла.
     * Используется в плагинах
     * @param file - объект файла с графом
     * @throws FileNotFoundException
     */
    public UndirectedGraph(File file) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
           e.printStackTrace();
        }

        this.vertexCount = scanner.nextInt();
        this.edgeCount = scanner.nextInt();
        this.vertices = new ArrayList<>();

        for (int i = 0; i < vertexCount; i++) {
            String name = scanner.next();
            this.vertices.add(new Vertex(
                    Integer.parseInt(name),
                    name,
                    Color.valueOf(scanner.next().toLowerCase()),
                    scanner.nextInt(),
                    scanner.next(),
                    new LinkedList<>()
            ));
        }

        for (int i = 0; i < edgeCount; i++) {
            Vertex v = this.vertices.get(Integer.parseInt(scanner.next()));
            Vertex w = this.vertices.get(Integer.parseInt(scanner.next()));

            UndirectedEdge undirectedEdge = new UndirectedEdge(
                    v,
                    w,
                    scanner.nextInt(),
                    Color.valueOf(scanner.next().toLowerCase()),
                    scanner.next(),
                    scanner.next()
            );

            this.vertices.get(Integer.parseInt(v.getName()))
                    .getEdgeList().add(undirectedEdge);
            this.vertices.get(Integer.parseInt(w.getName()))
                    .getEdgeList().add(undirectedEdge);
        }
    }

    @Override
    public String toString() {
        return "UndirectedGraph = {" + super.toString()
                + "\n}";
    }
}
