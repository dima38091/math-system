package com.mathsystem.plugin;

public enum PluginType {
    PROPERTY,
    CHARACTERISTIC
}
