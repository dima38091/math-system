package com.mathsystem.entity.task;

public enum AlgorithmType {
    PROPERTY,
    CHARACTERISTIC
}
