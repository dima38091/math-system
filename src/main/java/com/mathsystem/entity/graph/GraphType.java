package com.mathsystem.entity.graph;

public enum GraphType {
    DIRECTED,
    UNDIRECTED
}
