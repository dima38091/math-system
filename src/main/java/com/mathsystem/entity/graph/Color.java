package com.mathsystem.entity.graph;

public enum Color {
    red,
    pink,
    blue,
    green,
    yellow,
    brown,
    gray
}